const PDFDocument = require('pdfkit')
const fs = require('fs')
const base64 = require('file-base64')

coverLetterGenerator = async (senderInfo, contactInfo, content) => {
    let motivLetter = null
    const doc = new PDFDocument
    
    doc.pipe(fs.createWriteStream('cover.pdf'))
    
    doc.text(senderInfo.name, {align: 'left'})
    doc.text(senderInfo.adress1, {align: 'left'})
    if(senderInfo.adress2){
        doc.text(senderInfo.adress2, {align: 'left'})
    }
    doc.text(senderInfo.zipcode, {align: 'left'})
    doc.text(senderInfo.city, {align: 'left'})
    doc.text(senderInfo.mail, {align: 'left'})
    doc.text(senderInfo.phone, {align: 'left'})
    
    
    doc.text(contactInfo.name, {align: 'right'})
    doc.text(contactInfo.adress1, {align: 'right'})
    if(contactInfo.adress2){
        doc.text(contactInfo.adress2, {align: 'right'})
    }
    doc.text(contactInfo.zipcode, {align: 'right'})
    doc.text(contactInfo.city, {align: 'right'})
    
    doc.text('Madame, Monsieur,', {align: 'left'})
    doc.text(content.you, {align: 'left'})
    doc.text(content.me, {align: 'left'})
    doc.text(content.us, {align: 'left'})
    doc.text("Je vous prie d'agréer, Madame, Monsieur, l'expression de mes salutations distinguées", {align: 'left'})
    
    doc.text(senderInfo.name, {align: 'right'})
    
    
    doc.end()

    return true
}

module.exports = coverLetterGenerator