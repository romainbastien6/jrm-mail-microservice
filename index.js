const express = require('express')
const app = express()

const sgMail = require('@sendgrid/mail')
sgMail.setApiKey('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');

const bodyParser = require('body-parser')
const urlencodedParser = bodyParser.urlencoded({ extended: false })

const fetch = require('fetch-base64')
const coverLetterGenerator = require('./coverLetter')

app.all('/', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});


app.post('/', urlencodedParser, async (req, res, next) => {

   const cvURL = req.body.toSend.cv

   const senderInfo = req.body.toSend.senderInfo

    const contactInfo = req.body.toSend.contactInfo

    const content = {
        you: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vestibulum purus vel nisl mollis, vel gravida velit aliquet. Vestibulum non volutpat nulla, et convallis diam. Praesent malesuada urna et enim imperdiet feugiat. Sed vehicula accumsan magna ac pretium. Fusce sit amet mi ultricies, tempor dolor vitae, commodo magna. Proin volutpat neque eget elit iaculis, sit amet sodales neque vestibulum. Nullam accumsan nunc in nisi luctus, ut tempor sem imperdiet. Aenean blandit ex eu sapien vulputate porttitor. Fusce vehicula egestas bibendum.',
        me: 'Nunc posuere et lorem at vestibulum. Proin quam felis, vestibulum sollicitudin suscipit eu, lacinia ac purus. Cras bibendum ligula sed magna aliquam, sed pulvinar turpis euismod. Cras sollicitudin lorem quis placerat rhoncus. Vivamus dapibus massa ut dolor eleifend, et gravida lacus posuere. Morbi eget quam metus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis id turpis dui. Vivamus molestie tincidunt risus, et consectetur sem molestie in. Morbi lacus nulla, efficitur sed justo sed, pellentesque luctus orci.',
        us: 'Cras condimentum enim ac faucibus interdum. Pellentesque egestas pellentesque massa. Suspendisse quis maximus libero, sed aliquam lectus. Donec consectetur diam et dictum lobortis. Aenean malesuada dui diam, nec aliquet elit tincidunt eu. Nunc laoreet ligula vel nisi aliquam mollis. Nunc elementum egestas libero. Integer vitae lacus enim. Mauris quis consequat ex, nec venenatis mauris. Proin convallis at felis eu porttitor. Cras ac eros non dolor blandit iaculis. Aliquam et mi congue, tincidunt ligula non, maximus ex. Phasellus iaculis ex nec leo porttitor iaculis.'
    }

    const coverLetter = await coverLetterGenerator(senderInfo, contactInfo, content)

    const coverLetterBase64 = await fetch.local('./cover.pdf')

    const cv = await fetch.remote(cvURL)

    const msg = {
        to: contactInfo.mail,
        from: senderInfo.mail,
        subject: 'Candidature',
        text: req.body.toSend.mailTemplate,
        attachments: [
            {
                content: cv[0],
                filename: 'CVRomainBASTIEN.pdf'
            },
            {
                content: coverLetterBase64[0],
                filename: 'LettreDeMotivation.pdf',
                contentType: 'application/pdf'
            }
        ]
      };
    sgMail.send(msg)
      .then(() => {
          res.send('Mail sent')
      })
      .catch((err) => {
          res.send(err)
      })
})


app.listen(4000, () => {
    console.log('Mail microservice on port 4000')
})