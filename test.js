const coverLetterGenerator = require('./coverLetter')

const senderInfo = {
    name: 'Romain BASTIEN',
    adress1: '203 rue Louis Victor de Broglie',
    zipcode: 51430,
    city: 'Bezannes',
    mail: 'romainbastien6@gmail.com',
    phone: '0603615468'
}

const contactInfo = {
    name: 'Joe le Blaireau',
    adress1: '1, Sesame Street',
    zipcode: 66666,
    city: 'Orgrimmar',
    mail: 'romainbastien6@gmail.com',
    phone: '054564324'
}

const content = {
    you: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vestibulum purus vel nisl mollis, vel gravida velit aliquet. Vestibulum non volutpat nulla, et convallis diam. Praesent malesuada urna et enim imperdiet feugiat. Sed vehicula accumsan magna ac pretium. Fusce sit amet mi ultricies, tempor dolor vitae, commodo magna. Proin volutpat neque eget elit iaculis, sit amet sodales neque vestibulum. Nullam accumsan nunc in nisi luctus, ut tempor sem imperdiet. Aenean blandit ex eu sapien vulputate porttitor. Fusce vehicula egestas bibendum.',
    me: 'Nunc posuere et lorem at vestibulum. Proin quam felis, vestibulum sollicitudin suscipit eu, lacinia ac purus. Cras bibendum ligula sed magna aliquam, sed pulvinar turpis euismod. Cras sollicitudin lorem quis placerat rhoncus. Vivamus dapibus massa ut dolor eleifend, et gravida lacus posuere. Morbi eget quam metus. Interdum et malesuada fames ac ante ipsum primis in faucibus. Duis id turpis dui. Vivamus molestie tincidunt risus, et consectetur sem molestie in. Morbi lacus nulla, efficitur sed justo sed, pellentesque luctus orci.',
    us: 'Cras condimentum enim ac faucibus interdum. Pellentesque egestas pellentesque massa. Suspendisse quis maximus libero, sed aliquam lectus. Donec consectetur diam et dictum lobortis. Aenean malesuada dui diam, nec aliquet elit tincidunt eu. Nunc laoreet ligula vel nisi aliquam mollis. Nunc elementum egestas libero. Integer vitae lacus enim. Mauris quis consequat ex, nec venenatis mauris. Proin convallis at felis eu porttitor. Cras ac eros non dolor blandit iaculis. Aliquam et mi congue, tincidunt ligula non, maximus ex. Phasellus iaculis ex nec leo porttitor iaculis.'
}

const coverLetter = coverLetterGenerator(senderInfo, contactInfo, content)

console.log(coverLetter);